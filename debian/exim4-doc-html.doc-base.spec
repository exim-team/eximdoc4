Document: exim4-spec-html
Title: Exim specification
Author: Philip Hazel
Abstract: Exim (v4) reference manual. This is the html version.
Section: Network/Communication

Format: HTML
Index: /usr/share/doc/exim4-doc-html/spec_html/index.html
Files: /usr/share/doc/exim4-doc-html/spec_html/ch*.html /usr/share/doc/exim4-doc-html/spec_html/index*.html
